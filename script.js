var db = firebase.database().ref();
var messages = firebase.database().ref("chat/messages/");
var sendMessageForm = document.getElementById("messageForm");
var count = 0;
var chatContainer = document.getElementsByClassName("chat-room")[0];
var messageInput = document.getElementsByClassName("text-to-send")[0];
var loggedDetails = document.getElementById("display-logged-details");

function updateScroll(){
    chatContainer.scrollTo(0,chatContainer.scrollHeight);
}


function useForm (user) {	
	var newMessageRef = messages.push();
	newMessageRef.set({
		'text' : messageInput.value,
		'user_email' : user.email
	}).then(function() {
		updateScroll();
		messageInput.value = "";
	});
	
	return false;
}
firebase.auth().onAuthStateChanged(function(user) {
	var splashScreen = document.getElementById("splash-login");
	if (user) {
		loggedDetails.innerHTML = loggedDetails.innerHTML + user.email;
		sendMessageForm.addEventListener("submit", function (e) {
			useForm(user);
			e.preventDefault();
			return false;
		});
		splashScreen.style.zIndex = -2000;
		splashScreen.style.opacity = 0.0;
		db.on("value", function (snapshot) {
			snapshot.forEach(function (childSnapshot) {
				var chat = childSnapshot.val();
				var users = chat.users;
				var messages = chat.messages;
				
				var message_data = {};
				
				for (var id in messages) {
					
					message_data [id] = (messages[id]);
					//console.log(messages[id].user_id);
					if (messages[id].hasOwnProperty("user_email")) {
						message_data[id].author = messages[id].user_email;
					} else {
						message_data[id].author = users[messages[id].user_id].name;
					}
				}
				count = message_data.count;
				var htmlOutput = "";
				for(var id in message_data) {
					var rowClass = "row";
					if (user.email == message_data[id].author) {
						rowClass += " me";
					}
					htmlOutput += '<div class="' + rowClass + '"><div class="bubble col-xs-12"><span class="username">'+message_data[id].author+' ha scritto:</span>'+message_data[id].text+'</div></div>';
				}
				chatContainer.innerHTML = htmlOutput;
				updateScroll();
			})
		});
		document.getElementById("consequence-of-login").innerHTML = '<a href="#" onclick="logout(); return false;">Logout</a>';
	} else {
		console.log("AuthState: not logged");
		splashScreen.style.zIndex = 2000;
		splashScreen.style.opacity = 1.0;
		chatContainer.innerHTML = "";
	}
});
function login () {
	var user = document.getElementsByName("user")[0].value;
	var pwd = document.getElementsByName("pwd")[0].value;
	firebase.auth().signInWithEmailAndPassword(user, pwd).then(function () {
		document.getElementById("consequence-of-login").innerHTML = '<a href="#" onclick="logout(); return false;">Logout</a>';
		document.getElementById("errors-here").innerHTML = "";
	}).catch(function (e) {
		console.log(e);
		document.getElementById("errors-here").innerHTML = e.message;
	});
}
function logout () {
	firebase.auth().signOut().then(function() {
		document.getElementById("consequence-of-login").innerHTML = '';
		loggedDetails.innerHTML = "Logged as ";
	}).catch(function(e) {
	  console.log(e);
	});
}
function signin () {
	var user = document.getElementsByName("user")[0].value;
	var pwd = document.getElementsByName("pwd")[0].value;
	firebase.auth().createUserWithEmailAndPassword(user, pwd).then(function(response) {
		document.getElementById("errors-here").innerHTML = "";
	}).catch(function(error) {
	  document.getElementById("errors-here").innerHTML = error.message;
	});
}
